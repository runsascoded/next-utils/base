# @rdub/base

JS/TS utils; helper functions I've missed from standard libraries (and React)

<a href="https://npmjs.org/package/@rdub/base" title="@rdub/base on NPM"><img src="https://img.shields.io/npm/v/@rdub/base.svg" alt="@rdub/base NPM version" /></a>

- [a.tsx] - Anchor component wrapper (sets `target="_blank"` and `rel="noreferrer"` for external URLs).
- [arr.ts] - Array utils: `Arr`, `concat`, `isSorted`, `range`, `scan`, seedable `shuffle`, `sum`
- [color-scheme.tsx] - `useColorScheme` React hook for managing light/dark mode
- [console.ts] - import-able `time`, `timeEnd`, `log`, `warn`, `error`
- [dom.tsx] - element-Props aliases, partially-apply `className`s
- [heading.tsx]: `<H1>`…`<H6>` components: headings with configurable link-target offsets
- [json/load.ts], [json/fetch.ts]: `fetchJson`, `loadJson{,Sync}`
- [math.ts] - exports most of `Math`, also implements `clamp` and `interp`
- [objs.ts] - typed helpers for mapping between objects and arrays, over keys/values/entries
- [singleton.ts] - dedupe arrays, verify exactly one unique element, return it
- [state.ts] - `State<T, "name">` helper (`{ name: T, setName: (t: T) => void }`), `StateArr<T>` (`[T, Dispatch<T>]`), and aliases (`NS<T, Name>`, `S<T>`, etc.)
- [str.ts] - `titlecase` helper
- [time.ts] - `time`, `timeEnd`, `Timer`
- [use-set.ts] - `useSet`, `useOptSet` hooks for React

[a.tsx]: src/a.tsx
[arr.ts]: src/arr.ts
[color-scheme.tsx]: src/color-scheme.tsx
[console.ts]: src/console.ts
[dom.tsx]: src/dom.tsx
[heading.tsx]: src/heading.tsx
[json/load.ts]: src/json/load.ts
[json/fetch.ts]: src/json/fetch.ts
[math.ts]: src/math.ts
[objs.ts]: src/objs.ts
[singleton.ts]: src/singleton.ts
[state.ts]: src/state.ts
[str.ts]: src/str.ts
[time.ts]: src/time.ts
[use-set.ts]: src/use-set.ts
