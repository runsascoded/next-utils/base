import { createContext, useContext } from "react";

export const BasePathContext = createContext<string>("")

export function useBasePath() {
  return useContext(BasePathContext)
}
