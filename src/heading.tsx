import React, { CSSProperties, ReactNode } from "react";
import IntrinsicElements = React.JSX.IntrinsicElements;

export type Props = {
    H: keyof IntrinsicElements
    top?: string
    id: string
    className?: string
    style?: CSSProperties
    children: ReactNode
}

/**
 * Heading (h1, …, h6) with clicky IDs.
 *
 * Link target sits above the heading (with configurable offset `top`),
 * leaving room for sticky nav).
 *
 * @param H Heading ('h1', …, 'h6')
 * @param top Link target offset (default: "-4em")
 * @param id Heading `id` attribute (also becomes the URL "hash"/"fragment")
 * @param className optional CSS class
 * @param style optional CSS style
 * @param children Heading text / React nodes
 */
export function Heading({ H, top = "-4em", id, className, style, children }: Props)
{
    return <H
        style={{ position: "relative", ...(style ?? {}) }}
        className={className}
    >
        <span id={id} style={{ position: "absolute", top, }}></span>
        <a href={`#${id}`}>{children}</a>
    </H>
}

export const H1 = ({ children, ...props }: Omit<Props, 'H'>) => <Heading {...props} H="h1">{children}</Heading>
export const H2 = ({ children, ...props }: Omit<Props, 'H'>) => <Heading {...props} H="h2">{children}</Heading>
export const H3 = ({ children, ...props }: Omit<Props, 'H'>) => <Heading {...props} H="h3">{children}</Heading>
export const H4 = ({ children, ...props }: Omit<Props, 'H'>) => <Heading {...props} H="h4">{children}</Heading>
export const H5 = ({ children, ...props }: Omit<Props, 'H'>) => <Heading {...props} H="h5">{children}</Heading>
export const H6 = ({ children, ...props }: Omit<Props, 'H'>) => <Heading {...props} H="h6">{children}</Heading>

/**
 * Create a set of Heading components with some Props partially-applied.
 */
export function Headings(props: Omit<Props, 'H' | 'id' | 'children'>) {
    return {
        H1: (p: Omit<Props, 'H'>) => <H1 {...props} {...p} />,
        H2: (p: Omit<Props, 'H'>) => <H2 {...props} {...p} />,
        H3: (p: Omit<Props, 'H'>) => <H3 {...props} {...p} />,
        H4: (p: Omit<Props, 'H'>) => <H4 {...props} {...p} />,
        H5: (p: Omit<Props, 'H'>) => <H5 {...props} {...p} />,
        H6: (p: Omit<Props, 'H'>) => <H6 {...props} {...p} />,
    }
}
