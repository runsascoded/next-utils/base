import { ReactNode } from "react";

export type C<T = {}> = T & { children?: ReactNode }

export type O<T, Keys extends string> = Omit<T, Keys>

export type OC<T, Keys extends string = never> = O<T, 'children' | Keys>
