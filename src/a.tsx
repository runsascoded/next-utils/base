import React, { forwardRef } from "react"
import { AProps } from "./dom"

/**
 * Anchor component that sets target="_blank" and rel="noreferrer" for external URLs.
 */
const A = forwardRef<HTMLAnchorElement, AProps>((
  { href, children, ...attrs },
  ref
) => {
  if (href && (href.startsWith("/") || href.startsWith("#"))) {
    return (
      <a href={href} ref={ref} {...attrs}>
        {children}
      </a>
    );
  } else {
    const { target = "_blank", rel = "noreferrer", ...rest } = attrs;
    return (
      <a href={href} target={target} rel={rel} ref={ref} {...rest}>
        {children}
      </a>
    );
  }
})

export default A
