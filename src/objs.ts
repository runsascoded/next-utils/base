import { sum } from "./arr"

export function o2a<K extends Key, V, W>(o: Record<K, V>, fn: (k: K, v: V, idx: number) => W): W[] {
    return entries(o).map(([ k, v ], idx) => fn(k as K, v, idx))
}

export function order<K extends Key, V>(u: Record<K, V>): Record<K, V> {
    return keys(u).sort().reduce<Record<K, V>>(
        (o: Record<K, V>, k: K) => {
            o[k] = u[k];
            return o;
        },
        {} as Record<K, V>
    );
}

export function reorder<K extends string, T>(u: { [k in K]: T }, keys: K[]) {
    return keys.reduce(
        (o, k) => {
            o[k] = u[k];
            return o;
        },
        {} as { [k in K]: T }
    );
}

export function keys<K extends Key>(o: Record<K, any>): K[] {
    return Object.keys(o) as any
}

export function values<V>(o: { [k: Key]: V }): V[] {
    return Object.values(o)
}

export function sumValues(o: { [k: string]: number }) {
    return sum(values(o))
}

export type Key = string | number | symbol

export function entries<T extends Record<Key, unknown>>(o: T): [keyof T, T[keyof T]][] {
    return Object.entries(o) as [keyof T, T[keyof T]][]
}

export function fromEntries<K extends Key, V>(entries: Iterable<readonly [ K, V ]>): Record<K, V> {
    return Object.fromEntries(entries) as any
}

export function mapEntries<K1 extends Key, V1, K2 extends Key, V2>(o: Record<K1, V1>, fn: (k: K1, v: V1, idx: number) => [ K2, V2 ]): Record<K2, V2> {
    return fromEntries(entries(o).map(([ k, v ], idx) => fn(k, v, idx)))
}

export function mapValues<K extends Key, V1, V2>(o: Record<K, V1>, fn: (k: K, v: V1) => V2) {
    return mapEntries(o, ( k, t) => [ k, fn(k, t) ])
}

export function filterKeys<K extends Key, V>(o: Record<K, V>, fn: (k: K) => boolean): Record<K, V> {
    return fromEntries(entries(o).filter(([ k, t ]) => fn(k)))
}

export function filterEntries<K extends Key, V>(o: Record<K, V>, fn: (k: K, v: V) => boolean) {
    return fromEntries(entries(o).filter(([ k, t ]) => fn(k, t)))
}
