export function fetchJson<T>(url: string): Promise<T> {
  return fetch(url).then(data => data.json() as T)
}
