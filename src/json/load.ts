import { readFile } from "fs/promises";
import fs from "fs";
import path from "path";

export async function loadJson<T>(path: string): Promise<T> {
  try {
    const data = await readFile(path, 'utf-8');
    return JSON.parse(data) as T;
  } catch (err) {
    if (err instanceof Error) {
      const isReadError = 'code' in err && err.code === 'ENOENT';
      throw new Error(
        isReadError
          ? `File not found: ${path}`
          : `Failed to load JSON from ${path}: ${err.message}`
      );
    }
    throw err;
  }
}

export function loadJsonSync<T>(relPath: string): T {
  return JSON.parse(fs.readFileSync(path.join(process.cwd(), relPath), 'utf-8')) as T
}
