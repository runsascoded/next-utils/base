export const Arr = Array.from

export function batched<T>(array: T[], size: number): T[][] {
    if (size <= 0) throw new Error('Batch size must be greater than 0');
    const result: T[][] = [];
    for (let i = 0; i < array.length; i += size) {
        result.push(array.slice(i, i + size));
    }
    return result;
}

export function concat<T>(arrays: T[][]): T[] {
    return ([] as T[]).concat(...arrays)
}

export function isSorted<T>(vs: T[]): boolean {
    let prv: T | undefined | null = null
    for (let cur of vs) {
        if (prv !== undefined && prv !== null && cur < prv) return false
        prv = cur
    }
    return true
}

export function range(n1: number, n2?: number, n3?: number): number[] {
    if (n2 === undefined) {
        n2 = n1
        n1 = 0
    }
    const arr = []
    for (let i = n1; i < n2; i += n3 ?? 1) {
        arr.push(i)
    }
    return arr
}

export function scan<T, U>(array: T[], fn: (acc: U, val: T) => U, initial: U): U[] {
    const result: U[] = [initial];
    array.reduce((acc, val) => {
        const next = fn(acc, val);
        result.push(next);
        return next;
    }, initial);
    return result;
}

/**
 * Shuffles an array using the Fisher-Yates algorithm and a provided RNG
 * @template T The type of elements in the array
 * @param {T[]} array The array to shuffle
 * @param {() => number} rng A seedrandom random number generator
 * @returns {T[]} A new shuffled array
 */
export function shuffle<T>(array: T[], rng: () => number = Math.random): T[] {
    // Create a copy to avoid modifying the original array
    const result = [...array];

    // Fisher-Yates (Knuth) shuffle
    for (let i = result.length - 1; i > 0; i--) {
        // Generate random index between 0 and i (inclusive)
        const j = Math.floor(rng() * (i + 1));

        // Swap elements at indices i and j
        [result[i], result[j]] = [result[j], result[i]];
    }

    return result;
}

export function sum(arr: number[]) {
    return arr.reduce((a, b) => a + b, 0)
}
