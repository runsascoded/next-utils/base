import React from "react"
import { FC } from "react"
import { HTMLProps, SVGProps } from "react"

// HTML
export type AProps = HTMLProps<HTMLAnchorElement>
export type ButtonProps = HTMLProps<HTMLButtonElement>
export type DivProps = HTMLProps<HTMLDivElement>
export type InputProps = HTMLProps<HTMLInputElement>
export type ImgProps = HTMLProps<HTMLImageElement>
export type SpanProps = HTMLProps<HTMLSpanElement>

// SVG
export type SvgProps = SVGProps<SVGSVGElement>
export type GProps = SVGProps<SVGGElement>
export type PathProps = SVGProps<SVGPathElement>

// Partially apply className to Div and Span
export const Div: (className: string) => FC<DivProps> = (cls: string) => ({ children, className, ...props }) => <div className={className ? `${cls} ${className}` : cls} {...props}>{children}</div>
export const Span: (className: string) => FC<SpanProps> = (cls: string) => ({ children, className, ...props }) => <span className={className ? `${cls} ${className}` : cls} {...props}>{children}</span>
