export const { floor, ceil, sqrt, round, abs, max, min, pow, random, exp, log, log2, log10, cos, sin, tan, acos, asin, atan, atan2, cosh, sinh, acosh, asinh, E, LN2, PI, atanh, tanh, cbrt } = Math

export function clamp(num: number, min: number, max: number): number {
    return num <= min
        ? min
        : num >= max
            ? max
            : num
}

export function interp([x0, y0]: [number, number], [x1, y1]: [number, number]): (x: number) => number {
    return x => y0 + (y1 - y0) * (x - x0) / (x1 - x0)
}
