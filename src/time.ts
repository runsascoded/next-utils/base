
export type Id = string | undefined | null

export function time(id: Id, msg?: string) {
    if (id) {
        if (msg) {
            console.time(`${id}: ${msg}`)
        } else {
            console.time(id)
        }
    }
}

export function timeEnd(id: Id, msg?: string) {
    if (id) {
        if (msg) {
            console.timeEnd(`${id}: ${msg}`)
        } else {
            console.timeEnd(id)
        }
    }
}

export class Timer {
    id: Id
    msg?: string
    constructor(id: Id, msg?: string) {
        this.id = id
        this.msg = msg ?? id ?? undefined
    }
    start() {
        time(this.id, this.msg)
    }
    end() {
        timeEnd(this.id, this.msg)
    }
}
