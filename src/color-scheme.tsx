import { useCallback, useEffect, useMemo, useState } from 'react'
import "./color-scheme.css"
import useLocalStorageState from "use-local-storage-state";

export const DefaultColorSchemeKey = 'color-scheme'

export type ColorScheme = 'light' | 'dark'

export function getSystemScheme(): ColorScheme {
  return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'
}

export function useColorScheme(
  { key = DefaultColorSchemeKey, log: _log }: {
    key?: string
    log?: boolean
  } = {}
) {
  const [ colorScheme, _setColorScheme ] = useLocalStorageState<ColorScheme | null>(key, { defaultValue: getSystemScheme })
  const [ systemScheme, setSystemScheme ] = useState<ColorScheme>(getSystemScheme)

  const log = useCallback(
    (...args: any[]) => { if (_log) console.log(...args) },
    [_log]
  )

  const setScheme = useCallback(
    (colorScheme: ColorScheme | null) => {
      if (systemScheme === colorScheme) {
        _setColorScheme(null)
      } else {
        _setColorScheme(colorScheme)
      }
    }, [ systemScheme, ]
  )

  const curScheme = useMemo(() => colorScheme ?? systemScheme, [ colorScheme, systemScheme ])

  const toggleScheme = useCallback(
    () => setScheme(curScheme === 'light' ? 'dark' : 'light'),
    [curScheme]
  )

  log("colorScheme", colorScheme, "systemScheme", systemScheme)
  useEffect(
    () => {
      log("setting data-theme:", curScheme)
      document.documentElement.setAttribute('data-theme', curScheme)
    },
    [ curScheme, log ]
  )
  useEffect(
    () => {
      const handleChange = () => {
        const systemScheme = getSystemScheme()
        log("updating systemScheme:", systemScheme)
        setSystemScheme(systemScheme)
      }
      const mediaQuery = window.matchMedia('(prefers-color-scheme: dark)')
      mediaQuery.addEventListener('change', handleChange)
      return () => { mediaQuery.removeEventListener('change', handleChange) }
    },
    [ setSystemScheme, log ]
  )

  return { colorScheme, systemScheme, curScheme, setScheme, toggleScheme }
}
