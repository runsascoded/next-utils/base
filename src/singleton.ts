
export default function singleton<T>(ts: T[]): T {
    const set = new Set(ts)
    if (set.size !== 1) {
        throw new Error(`expected one unique element, found ${set.size} (originally ${ts.length}): ${ts.join(', ')}`)
    }
    return set.values().next().value as T
}

export function singletonOpt<T>(ts: T[]): T | null {
    const set = new Set(ts)
    return (set.size === 1) ? set.values().next().value as T : null
}

export const solo = singleton
