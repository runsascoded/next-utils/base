// e.g. `function Foo({ num, setNum }: State<number, 'num'>) { ... }`
// or `function Foo([ num, setNum ]: S<number>) { ... }`
import { Dispatch } from "react"

export type State<T, Name extends string> = {
  [K in Name]: T
} & {
  [K in `set${Capitalize<Name>}`]: (value: T) => void
}

export type NamedState<T, Name extends string> = State<T, Name>
export type NS<T, Name extends string> = NamedState<T, Name>

export type StateArr<T> = [ T, Dispatch<T> ]
export type SA<T> = StateArr<T>
export type S<T> = StateArr<T>

export type StateObj<T> = { val: T, set: Dispatch<T> }
export type SO<T> = StateObj<T>
